import 'react-app-polyfill/ie9';
import 'react-app-polyfill/stable';
import 'core-js/es/object/entries';
import 'core-js/features/object/entries';
import Promise from 'promise-polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';

if (!window.Promise) {
  window.Promise = Promise;
}
if (!Object.entries) {
  Object.entries = function(obj) {
    const ownProps = Object.keys(obj);
    let i = ownProps.length;
    const resArray = new Array(i); // preallocate the Array
    while (i) {
      resArray[i] = [ownProps[i], obj[ownProps[i]]];
      i -= 1;
    }
    return resArray;
  };
}
ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA

