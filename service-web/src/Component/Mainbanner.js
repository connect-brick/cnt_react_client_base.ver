import React from 'react'
import '../css/Mainbanner.css';
import { Container,Col } from 'react-bootstrap';

function Mainbanner(){

	// 커넥트 브릭 서비스 이용하기 누르면 동작
	const onPageChange = () => {
		window.location.href="http://service.connectbrick.com/main";
	}

    return(
			<div className="main">
				<svg className="bg_main_banner">
					<linearGradient id="bg_banner_u" spreadMethod="pad" x1="0.5" x2="0.5" y1="0.761" y2="1">
						<stop offset="0" stopColor="#f4f5fd" stopOpacity="1"></stop>
						<stop offset="1" stopColor="#eff1fa" stopOpacity="1"></stop>
					</linearGradient>
					<rect id="bg_main_banner" rx="0" ry="0" x="0" y="0" width="100%" height="716">
					</rect>
				</svg>
				<Container>
					<Col>
						<div id="___3___">
							<span>내가 만드는 공간<br/>인테리어 3면 자재 마켓,<br/>커넥트브릭</span>
						</div>
						<div id="_______________">
							<span>혼자 시작하는 셀프 인테리어 너무 어려운가요?<br/>커넥트브릭에서 자재 선택부터 시공까지 쉽게 도와드립니다.</span>
						</div>

						<img src="/img/mainbannerImg/image_platform.png" alt="main_profile" style={{width:'784px', height:'580px'}} className="mainbanner-icon" />

						<div className="b_website_se">
							<div className="b_website_service">
								<span onClick={onPageChange}>커넥트브릭 서비스 이용하기</span>
							</div>
						</div>
					</Col>
				</Container>	
		</div>
    );
}

export default Mainbanner;