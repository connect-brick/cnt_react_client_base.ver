import React from 'react'
import '../css/Section6.css';
import { Container,Col } from 'react-bootstrap';

function Section6(){
    return(
    <div className="section6">
		<svg className="bg_darkgray">
			<rect id="bg_darkgray" rx="0" ry="0" x="0" y="0" width="100%" height="585px">
			</rect>
		</svg>
		<Container>		
			<Col>
				<div id="_8733">
					<div id="_______Master________WMSWareho">
						<span>커넥트브릭 올빗 애널리시스는 건축마감재 데이터를 <br/>자재 Master 정보와 특성정보, 시공특성 및 추가 필요정보로 분석하여<br/>WMS</span><span style={{fontSize:"13.5px"}}>(Warehouse Management System)</span><span>를 구축해 시스템화하고 있습니다.</span>
					</div>
					<div id="Connect_Brick_Orbit_Analysis">
						<span>Connect Brick<br/>Orbit Analysis</span>
					</div>
				</div>
				<img src="/img/section6Img/image_orbit.png" alt="sec6_profile" className="section6_img" />
			</Col>	
		</Container>	
	</div>
    );
}
export default Section6;