import React from 'react'
import { Container,Col } from 'react-bootstrap';
import '../css/PartnerSection.css';
function PartnerSection(){
    return(
        <div className="PartnerSection">
        	<svg className="bg_pblue">
			    <rect id="bg_pblue" rx="0" ry="0" x="0" y="0" width="100%" height="1469">
			    </rect>
		    </svg> 

            <Container>
                <Col>
                    <div id="__my">
                        <span>커넥트브릭 파트너스 모집</span>
                    </div>
                    <div id="________________mx">
                        <span>커넥트브릭은 자재를 전문적으로 믿고 맡길 수 있는 인테리어 업체들과 함께합니다.<br/>커넥트브릭 서비스를 통해  고객을 효율적으로 만나고 수익을 창출해보세요.</span>
                    </div>

                    <img src="/img/PartnerSectionImg/image_partner.png" className="partnerSec_img" />
                    <img src="/img/PartnerSectionImg/icon_partner_1.png" className="partner_logo1" />
                    <img src="/img/PartnerSectionImg/icon_partner_2.png" className="partner_logo2"/>
                    <img src="/img/PartnerSectionImg/icon_partner_3.png" className="partner_logo3"/>
                    <img src="/img/PartnerSectionImg/icon_partner_4.png" className="partner_logo4"/>

                    <div id="_____m">
                        <span>고객 매칭 / 견적서 작성</span>
                    </div>
                    <div id="___m"> 
                        <span>실시간 현황 관리</span>
                    </div>
                    <div id="___na">
                        <span>토탈인테리어 확장성</span>
                    </div>
                    <div id="____m">
                        <span>커넥트브릭 자재 직구매</span>
                    </div>
                    <div id="____________m">
                        <span>업체와 고객을 시스템이 자동으로 계산해 매칭<br/>합니다. 매칭된 고객들의 예상견적서를 간편하<br/>게 작성하고 관리해보세요.</span>
                    </div>
                    <div id="____________na">
                        <span>매칭된 모든 고객들의 견적서 승인, 결제, 자재<br/>배송 및 시공스케줄을 실시간으로 확인하고 수<br/>립할 수 있습니다.</span>
                    </div>
                    <div id="____________nb">
                        <span>고객에게 꼭 필요한 추가적인 인테리어 시공들<br/>을 상담을 통해 도와줄 수 있습니다.고객님들의<br/>인테리어 고민을 함께 해결해주세요.</span>
                    </div>
                    <div id="____________nc">
                        <span>커넥트브릭에서 판매하는 모든 자재들의 재고<br/>현황을 간편하게 확인하고 합리적인 가격으로<br/>직접 구매해 보세요.</span>
                    </div>
                    <img className="partner_main_img" src="img/PartnerSectionImg/image_partner_screen.png"/>
                </Col>	
		    </Container>	
        </div>
    );
}
export default PartnerSection;