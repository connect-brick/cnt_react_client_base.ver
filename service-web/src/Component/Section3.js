import React  from 'react'
import '../css/Section3.css';
import { Container,Col } from 'react-bootstrap';

function Section3(){
    return(
        <div className="section3">
            <svg className="bg_gray_ba">
                <rect id="bg_gray_ba" rx="0" ry="0" x="0" y="0" width="100%" height="894">
                </rect>
            </svg>
			<Container >	
            		<Col>
                        <div id="______bc">
                            <span>공간카드 속 자재들을 그대로 주문</span>
                        </div>
                        <div id="______________NO_bb">
                            <span>맘에드는 인테리어 스타일의 공간카드를 찾았다면<br/>바닥, 벽, 천장의 시공 면적을 입력해 카드별 예상 구매가를 비교해 보세요.</span>
                        </div>
                        <img src={"/img/section3Img/image_detail_card.png"} alt="sec3-profile" className="image_detail" />
		        	</Col>	
		    </Container>	
        </div>
    );
}
export default Section3;