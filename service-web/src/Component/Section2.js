import React,{useState, createRef} from 'react'
import '../css/Section2.css';
import { Container,Col } from 'react-bootstrap';
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Card from 'react-bootstrap/Card';

function Section2(){
    
  const [centerRef, setCenterRef] = useState(()=>createRef())
  const [mainCard, setMainCard]= useState(()=>createRef())
  // multiple slider src
  const slidesData = [
    {
      id: 1,
      src:'/img/section2Img/card_1_b.png'
    }, 
    {
      id: 2,
      src:'/img/section2Img/card_2_b.png'

    }, {
      id: 3,
      src:'/img/section2Img/card_3_b.png'

    }, {
      id: 4,
      src:'/img/section2Img/card_4_b.png'

    }, {
      id: 5,
      src:'/img/section2Img/card_5_b.png'

    },
     {
      id: 6,
      src:'/img/section2Img/card_6_b.png'

    },
    {
      id: 7,
      src:'/img/section2Img/card_7_b.png'

    },
  ];
  // single slider src
  const mainCardSrc =[
    {
      id:7,
      src:'/img/section2Img/card_7_a.png'
    },
    {
      id:1,
      src:'/img/section2Img/card_1_a.png'
    },
    {
      id:2,
      src:'/img/section2Img/card_2_a.png'
    },
    {
      id:3,
      src:'/img/section2Img/card_3_a.png'
    },
    {
      id:4,
      src:'/img/section2Img/card_4_a.png'
    },
    {
      id:5,
      src:'/img/section2Img/card_5_a.png'
    },
    {
      id:6,
      src:'/img/section2Img/card_6_a.png'
    },
  ]
    // multiple slider setting
    const settings ={
        infinite:true,
        dots:false,
        fade:false,
        variableWidth:false,
        slidesToShow: 6,
        arrows:false,
        slidesToScroll:1,
        waitForAnimate:true,
        centerPadding:9,
        autoplay:true,
        centerMode:true,
        pauseOnFocus:false,
        slickPause:false,
        pauseOnHover:false,
        speed:2500,
        autoplaySpeed:3500,
        cssEase:'ease',
        beforeChange : (oldIndex,currentIndex) =>{
          current(currentIndex-1);
        },
    }
    // single slider setting
    const settings2 ={
      autoplay:true,
      fade:true,
      slidesToShow:1,
      speed:2500,
      autoplaySpeed:3500,
      arrows:false,
      pauseOnHover:false,
      pauseOnFocus:false,
    }

    // className='mainCard' 의 image 교체를 위해 multiple slider 의 src를  받아와 적용
    const current = (currentIndex) => {
      const beforeCard = document.querySelector('.main-card2');
      const track = beforeCard.querySelector('.slick-track');
      const mCard = track.querySelector('div[data-index="'+Number(currentIndex+1)+'"]');
      const card = mCard.querySelector('.mcard');
      const cardSrc = card.src;
      changeImage(cardSrc);
    }

    const changeImage = (paramSrc) => {  
      mainCard.current.src = paramSrc;
    }
    
    return(
        <div className="section2">
            <svg className="bg_white_c">
                <rect id="bg_white_c" rx="0" ry="0" x="0" y="0" width="100%" height="894">
                </rect>
        		</svg>
            <Container>
              <Col>
                <div id="_____c">
                    <span>맘에 드는 인테리어 공간카드가 가득</span>
                </div>
                <div id="_________________">
                    <span>커넥트브릭 전문에디터들이 직접 자재를 조합해 카드를 제작합니다.<br/>트렌드가 반영된 다양한 스타일의 공간카드를 구경해 보세요.</span>
                </div>
         
                {/* centerCard fade 효과 적용시 잔상 생기는것 막기위해*/}
                <div className="main-card">           
                    <Card.Img className="mcard" ref={mainCard} src={`/img/section2Img/card_1_a.png`}  />        
                </div>

                {/* single slider */}
                <div className="main-card2">
                  <Slider {...settings2}  >
                      {mainCardSrc.map((slide,id)=> 
                      <Card.Img className="mcard" key={id}  src={slide.src}/>
                    )}
                  </Slider>
                </div>
                  {/* multiple slider */}
                  <div className="main-slider" ref={centerRef}>
                  <Slider {...settings}   >
                      {slidesData.map((slide,id)=> 
                        <Card.Img className="slideImg" src={slide.src} key={id} style ={{width:'187px', height:'251px', objectFit:'cover', boder:'1px solid white'}} />
                      )}
                  </Slider>
                </div>
              </Col>	
		         </Container>	
        </div>
    );
}

export default Section2;