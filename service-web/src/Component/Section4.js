import React from 'react'
import '../css/Section4.css';
import { Container,Col } from 'react-bootstrap';

function Section4(){

    const sec4LeftCardImg  = [
        {
            id:1,
            src:"/img/section4Img/tile_card_a1.png"
        },
        {
            id:2,
            src:"/img/section4Img/tile_card_a2.png"
        },
        {
            id:3,
            src:"/img/section4Img/tile_card_a3.png"
        },
        {
            id:4,
            src:"/img/section4Img/tile_card_a4.png"
        },
        {
            id:5,
            src:"/img/section4Img/tile_card_a5.png"
        },
        {
            id:6,
            src:"/img/section4Img/tile_card_a6.png"
        },
        {
            id:7,
            src:"/img/section4Img/tile_card_a7.png"
        }
    ]

    const sec4CenterCardImg = [
        {
            id:1,
            src:"/img/section4Img/tile_card_b1.png"
        },
        {
            id:2,
            src:"/img/section4Img/tile_card_b2.png"
        },
        {
            id:3,
            src:"/img/section4Img/tile_card_b3.png"
        },
        {
            id:4,
            src:"/img/section4Img/tile_card_b4.png"
        },
        {
            id:5,
            src:"/img/section4Img/tile_card_b5.png"
        },
        {
            id:6,
            src:"/img/section4Img/tile_card_b6.png"
        },
        {
            id:7,
            src:"/img/section4Img/tile_card_b7.png"
        },
    ]

    const sec4RightCardImg =[
        {
            id:1,
            src:"/img/section4Img/tile_card_c1.png"
        },
        {
            id:2,
            src:"/img/section4Img/tile_card_c2.png"
        },
        {
            id:3,
            src:"/img/section4Img/tile_card_c3.png"
        },
        {
            id:4,
            src:"/img/section4Img/tile_card_c4.png"
        },
        {
            id:5,
            src:"/img/section4Img/tile_card_c5.png"
        },
        {
            id:6,
            src:"/img/section4Img/tile_card_c6.png"
        },
        {
            id:7,
            src:"/img/section4Img/tile_card_c7.png"
        },
    ]
    return(
    <div className="section4">
		<svg className="bg_white">
			<rect id="bg_white" rx="0" ry="0" x="0" y="0" width="100%" height="894">
			</rect>
		</svg>
		<Container>
            <Col>
                <div id="_____1">
                    <span>어려운 인테리어 자재,이제 스마트하게</span>
                </div>
                <div id="section_4_1">
                    <span>커넥트브릭 자재 데이터를 기반으로 인테리어 트렌드를 반영하고<br/>품질부터 가격까지 엄선된 자재들만 판매합니다.</span>
                </div>
                <div id="living">
                    <div className="living-text">
                        <span>거실</span>
                    </div>
                </div>
                <div id="bedroom">
                    <div className="bedroom-text">
                        <span>침실</span>
                    </div>
                </div>
                <div id="bath">
                    <div className="bath-text">
                        <span>욕실</span>
                    </div>
                </div>

                <div className="background">
                    <div className="inner-card">
                        {/* 거실 */}
                        <div className="section4_11">
                                {sec4LeftCardImg.map((slide,id)=>
                                    <div className="card-box" key={id}>
                                        <img src={slide.src} alt="..." className="sec4Card"/>
                                    </div>                    
                                )}
                            </div>
                            <div className="section4_12">
                                {sec4LeftCardImg.map((slide,id)=>
                                    <div className="card-box" key={id}>
                                        <img src={slide.src}  alt="..." className="sec4Card"/>
                                    </div>
                                )}
                            </div>
                            {/* 침실*/}
                            <div className="section4_21">
                                {sec4CenterCardImg.map((slide,id)=>
                                    <div className="card-box" key={id}>
                                        <img src={slide.src} alt="..." className="sec4Card"/>
                                    </div>                    
                                )}
                            </div>
                            <div className="section4_22">
                                {sec4CenterCardImg.map((slide,id)=>
                                    <div className="card-box" key={id}>
                                        <img src={slide.src} alt="..." className="sec4Card"/>
                                    </div>                    
                                )}
                            </div>
                            {/* 욕실 */}
                            <div className="section4_31">
                                {sec4RightCardImg.map((slide,id)=>
                                    <div className="card-box" key={id}>
                                        <img src={slide.src} alt="..."  className="sec4Card"/>
                                    </div>                    
                                )}
                            </div>
                            <div className="section4_32">
                                {sec4RightCardImg.map((slide,id)=>
                                    <div className="card-box" key={id}>
                                        <img src={slide.src} alt="..." className="sec4Card"/>
                                    </div>                    
                                )}
                        </div>
                    </div>
                 </div>
			</Col>	
		 </Container>	
	</div>
    )
}

export default Section4;