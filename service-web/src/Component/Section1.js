import React from 'react'
import '../css/Section1.css';
import { Container,Col } from 'react-bootstrap';
function Section1(){
    return(
		<div className="section1">	
			<svg className="bg_darkgray_e">
				<rect id="bg_darkgray_e" rx="0" ry="0" x="0" y="0" width="100%" height="820">
				</rect>
			</svg>
			<Container>
            	<Col>
					<div id="______3_">
						<span>타일·벽지를 패키지로 배송<br/>가격 거품은 빼고 3면은 채우다</span>
						</div>
						<div id="___________e">
						<span>커넥트브릭은 중간 유통과 가격 거품을 걷어낸 인테리어 자재들을 패키지로 배송합니다.</span>
					</div>
					<div id="___e">
						<span>인테리어 자재 조합</span>
					</div>
					<div id="_____________e">
						<span>자재의 특성, 용도, 디자인 및 트렌드를 기<br/>반으로 조합된 공간카드를 통해 쉽게 인테<br/>리어 자재를 선택할 수 있습니다.</span>
					</div>
					<div id="___ex">
						<span>자재 패키지 배송</span>
					</div>
					<div id="__________">
						<span>선택한 카드의 자재들이 시공시 꼭 필요한<br/>부자재, 시공매뉴얼과 함께 패키지로 원하<br/>는 날짜에 배송됩니다.</span>
					</div>
					<div id="___es">
						<span>인테리어 업체 매칭</span>
					</div>
					<svg className="line_eqw" viewBox="0 0 1 297.46">
						<path id="line_eqw" d="M 0 0 L 0 297.4599609375">
						</path>
					</svg>
					<svg className="line_eq" viewBox="0 0 1 297.46">
						<path id="line_eq" d="M 0 0 L 0 297.4599609375">
						</path>
					</svg>
					<div id="____________">
						<span>숙련도 및 시공품질을 믿고 맡길 수 있는 업<br/>체들만 연결합니다. 전문 시공자가 필요하<br/>다면 커넥트브릭 파트너스를 만나보세요.</span>
					</div>
					<img src="/img/section1Img/icon_sec1_1.png" alt="sec1_profile" className="icon_sec1_1" />
					<img src="/img/section1Img/icon_sec1_2.png" alt="sec1_profile" className="icon_sec1_2" />
					<img src="/img/section1Img/icon_sec1_3.png" alt="sec1_profile" className="icon_sec1_3" />
				</Col>	
			 </Container>	
		</div>
    )
}
export default Section1;