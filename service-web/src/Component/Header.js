import React from 'react'
import '../css/Header.css';
import { Container,Col } from 'react-bootstrap';



function Header(){
	const onPageChange = (event) => {
		event.preventDefault();
		let el = document.querySelector('.dataInput');
		el.scrollIntoView(true);
		window.scrollBy(0,50);
		// document.querySelector('.main').offset();

	}
	const onMainChange =(event) =>{
		event.preventDefault();
		document.querySelector('.main').scrollIntoView(true);
		window.scrollBy(0,-90);

		// document.querySelector('.main').offset();
	}

    return(
		<div className="header ">
        	<div id="header_1">	
				<div id="bg_header" className="bg_header">
					<svg className="bg_header_fi">
						<rect id="bg_header_fi" rx="0" ry="0" x="0" y="0">
						</rect>
					</svg>
				</div>
			<Container>
				<Col>
					<div id="_fk">
						<span><a href="http://service.connectbrick.com/access/login">로그인</a></span>
					</div>
					<div id="_f">
						<span><a href="http://service.connectbrick.com/access/join">회원가입</a></span>
					</div>
					<img src="/img/HeaderImg/logo_cnt_blue.svg" className="logo_cnt_blue" onClick={onMainChange} />

					<div onClick={onPageChange} className="b_partner">
						<div id="_partner">
							<span>파트너 신청하기</span>
						</div>
					</div>
				</Col>	
			</Container>	
			</div>
		</div>
    );
}

export default Header;