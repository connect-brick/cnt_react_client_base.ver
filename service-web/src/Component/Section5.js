import React from 'react'
import '../css/Section5.css';
import { Container,Col } from 'react-bootstrap';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
function Section5(){

    return(
		<div className="section5">
			<svg className="bg5_darkgray">
				<rect id="bg5_darkgray" rx="0" ry="0" x="0" y="0" width="100%" height="673">
				</rect>
			</svg>
			 <Container> 
            		<Col>
						<div id="___c">
								<span>커넥트브릭 반셀프 이용방법</span>
						</div>
						<img src="/img/section5Img/icon_step_1.png" alt="profile" className="icon_step1"/>
						<img src="/img/section5Img/icon_step_2.png" alt="profile" className="icon_step2"/>
						<img src="/img/section5Img/icon_step_3.png" alt="profile" className="icon_step3"/>
						<img src="/img/section5Img/icon_step_4.png" alt="profile" className="icon_step4"/>
						<img src="/img/section5Img/icon_step_5.png" alt="profile" className="icon_step5"/>

						<div id="__da">
							<span>1. 서비스 주문</span>
						</div>
						<div id="__db">
							<span>2. 무료 견적</span>
						</div>
						<div id="_dc">
							<span>3. 승인·결제</span>
						</div>
						<div id="__dd">
							<span>4. 자재 배송</span>
						</div>
						<div id="__de">
							<span>5. 시공 시작</span>
						</div>
						<img src="/img/section5Img/icon_arrow.png" alt="profile" className="arrow1 arrow" />
						<img src="/img/section5Img/icon_arrow.png" alt="profile" className="arrow2 arrow" />
						<img src="/img/section5Img/icon_arrow.png" alt="profile" className="arrow3 arrow" />
						<img src="/img/section5Img/icon_arrow.png" alt="profile" className="arrow4 arrow" />
					
						<div id="_____">
							<span>구매를 원하는 공간카드의 자<br/>재패키지 주문서를 작성해 서<br />비스를 주문합니다.</span>
						</div>
						<div id="____">
							<span>정확한 자재구매를 위한 전화<br/>상담, 방문실측 및 예상견적<br/>서 작성을 진행합니다.</span>
						</div>
						<div id="______">
							<span>마이 페이지에서 예상 견적서<br/>를 확인하고 최종 견적서 승<br/>인 및 결제를 진행합니다.</span>
						</div>
						<div id="_____cm">
							<span>결제가 완료되면 자재 패키지<br/>가 원하는 날짜에 시공 장소<br/>로 배송됩니다.</span>
						</div>
						<div id="_____cn">
							<span>자재 패키지가 도착되면 매칭<br/>되었던 인테리어 업체가 시공<br/>을 시작합니다.</span>
						</div>
					</Col>	
			</Container>
		</div>
    );
}
export default Section5;