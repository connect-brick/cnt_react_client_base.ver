import React, { useEffect, useState } from 'react'
import '../css/DataInput.css'
import { Container,Col, Form,Button, FormGroup } from 'react-bootstrap';
import axios from 'axios'


function DataInput(){
    const [validated, setValidated] = useState(false);  //유효성 state
    const [companyName, setCompanyName] = useState(''); //회사이름
    const [name, setName] = useState('');               //담당자 이름
    const [telNumber, setTelNumber] = useState('');     //휴대폰번호
    const [isEmail, setIsEmail] = useState('');         //이메일
    const [question, setQuestion] = useState('');       //문의 내용
    const [area, setArea] = useState('');               //지역명
    const [privacy,setPrivacy]= useState(false);        //개인정보수집 체크

    useEffect(()=>{     // email input값 변경될때마다 inputcheck함수실행
        inputCheck();
    },[isEmail])                 
    useEffect(()=>{
        inputCheck();
    },[telNumber])     // telnumber input값 변경될때마다 inputcheck함수실행
    useEffect(()=>{
        inputCheck();
    },[privacy]);     // 개인정보수집 체크 변경될때마다 inputcheck함수실행

    useEffect(()=> {       // 연락처 11자리 입력하면 자동 (-) 삽입
        if (telNumber.length === 11) {
            setTelNumber(telNumber.replace(/-/g, '').replace(/(\d{3})(\d{4})(\d{4})/, '$1-$2-$3'));
        }
    },[telNumber]);

    const companyNameChange = (event) =>{    
        setCompanyName(event.currentTarget.value);
    }
    const nameChange =(event) => {
        setName(event.currentTarget.value);
    }
    const questionChange = (event) => {
        setQuestion(event.currentTarget.value);
    }
    const telNumberChange =(event) => {
        setTelNumber(event.currentTarget.value);
    }
    const areaonChange =(event) => {
        setArea(event.currentTarget.value);
    }
    const emailChange =(e) => {
        setIsEmail(e.currentTarget.value); 
    }  
    // 개인정보 수집 체크되있으면 feedback 제거, 안되있으면 feedback 추가
    const privacyChange = (event) => {
        setPrivacy(event.target.checked);
        let feedback = document.querySelector('.checkbox_feedback');
        if(event.target.checked===true){
            feedback.classList.remove('visi');
        }else if(event.target.checked===false){
            feedback.classList.add('visi');
        }
    }

    // 개인정보 수집 및 이용 text 누르면 새창에 해당 경로로 이동
    const changePrivacyPage =() => {
        let openNewWindow = window.open("about:blank");
        openNewWindow.location.href="http://service.connectbrick.com/service/policy/partner";
    }

    // 연락처, 이메일 둘중 하나만 받으면 validation true 가 되게하기위한함수.
    const inputCheck = () => {
        const phone = document.querySelector('.input3');
        const email = document.querySelector('.input4');
        let phoneFeedback = document.querySelector('.input_13');
        let emailFeedback = document.querySelector('.input_14');
        let phoneInput = phone.value;
        let emailInput = email.value;

        //휴대폰, 이메일 중 하나면 입력하면 require속성 제거 
        if(!phoneInput){
            if(!emailInput){
                phoneFeedback.innerText=""
                emailFeedback.innerText="연락처/이메일 중 하나는 필수 입력 사항입니다."
                email.setAttribute('required','')
                phone.setAttribute('required','')
            }else{
                phone.removeAttribute('required')
                emailFeedback.innerText="이메일 형식이 올바르지 않습니다."
            }
        }else{
            if(!phoneInput){
                phoneFeedback.innerText=""
                emailFeedback.innerText="연락처/이메일 중 하나는 필수 입력 사항입니다.."
            }else{

                email.removeAttribute('required');
            }
        }
    }

    // 신청하기 누르면 동작하는 함수 , 유효성 검사 체크하여 form data 전송
    function submitData(event){
        const a = document.querySelector('.form-check-input');
        const feed = document.querySelector('.checkbox_feedback');
        if(a.value==='false'){
            feed.classList.add('visi');
        }
        const form = event.currentTarget;
        inputCheck();

        // 유효성 체크
        if(form.checkValidity()===false){
            event.preventDefault();
            event.stopPropagation();
        }
        setValidated(true);

        let newform = new FormData();
        newform.append('companyName',companyName)
        newform.append('offerer',name)
        newform.append('tel',telNumber)
        newform.append('email',isEmail)
        newform.append('additional',question)
        newform.append('area',area)
        newform.append('privacy',privacy)

        axios.post(`https://www.connectbrick.com:8443/service/offer/partner/submit`, newform)
        .then(response=>{
            alert('접수가 완료되었습니다.')
            window.location.href="/"
        }).catch(error=>{           
            if(error.response.status===400){
                alert('필수항목을 입력해 주세요.');
            }
            else if(error.response.status===404){
               alert('다시 입력해 주세요.');
            }
        })
    }


    return(
        <div className="dataInput" id="dataInput">
            <svg className="dataInput_background">
                <rect id="dataInput_background" rx="0" ry="0" x="0" y="0" width="100%" height="1115">
                </rect>
            </svg>
            <Container>
		         <Col>
                    <div id="dataInput-text">
                        <span>커넥트브릭 파트너 신청</span>
                    </div>
                    <Form 
                    noValidate 
                    validated={validated}
                    >
                        <Form.Group className="input_1">
                            <Form.Control
                                required
                                type="text"
                                name="companyName"
                                placeholder="업체명을 입력해 주세요."
                                style={{width:"421px", height:"50px"}}
                                onChange={companyNameChange}
                                value={companyName}
                            />
                            <Form.Control.Feedback type="invalid" className="input_11">업체명은 필수 입력 사항입니다.</Form.Control.Feedback>
                        </Form.Group>

                        <Form.Group className="input_2">
                             <Form.Control                     
                                type="text"
                                placeholder="지역명을 입력해주세요."
                                required
                                onChange={areaonChange}
                                value={area}
                                name="area"
                                style={{width:"421px", height:"50px"}}
                            />
                            <Form.Control.Feedback type="invalid" className="input_16">지역명은 필수 입력 사항입니다.</Form.Control.Feedback>
                        </Form.Group>
                   
                        <Form.Group className="input_3">
                            <Form.Control
                                onChange={nameChange}
                                value={name}
                                type="text"
                                name="offerer"
                                placeholder="담당자 이름을 입력해주세요."
                                style={{width:"421px", height:"50px"}}
                            />
                        </Form.Group>
                        <Form.Group className="input_4">
                            <Form.Control
                                className="input3"
                                type="text"
                                required
                                name="tel"
                                placeholder="연락처를 입력해주세요."
                                onChange={telNumberChange}
                                value={telNumber}
                                style={{width:"421px", height:"50px"}}
                            />
                            <Form.Control.Feedback type="invalid" className="input_13">연락처를 입력해주세요.</Form.Control.Feedback>
                        </Form.Group>

                        <Form.Group className="input_5">
                            <Form.Control
                                    onChange={questionChange}
                                    value={question}
                                    as="textarea"
                                    rows={15}
                                    name="additional"
                                    placeholder="문의 내용을 남겨주세요."
                                    style={{width:"421px", height:"217px"}}
                                />
                        </Form.Group>
                        
                        <Form.Group className="input_6">
                            <Form.Control   
                                    className="input4"
                                    onChange={emailChange}
                                    name="email"
                                    type="email"
                                    value={isEmail} 
                                    required
                                    placeholder="이메일 주소를 입력해주세요."
                                    style={{width:"421px", height:"50px"}}
                                />
                            <Form.Control.Feedback type="invalid" className="input_14">이메일 형식이 올바르지 않습니다.</Form.Control.Feedback>
                        </Form.Group>

                        <FormGroup className="input_7">
                            <Form.Check              
                                    required
                                    type="checkbox"
                                    feedback="동의 여부란에 체크해주세요"
                                    style={{color:'#BFBFBF'}}
                                    name="privacy"
                                    value={privacy}
                                    onChange={privacyChange}                                    
                            />
                            <span className="checkbox_feedback">개인정보 수집 및 이용에 동의가 필요합니다.</span>
                            <Form.Label className="input_privacy">
                                <span id="privacy_text" onClick={changePrivacyPage}>개인정보 수집 및 이용</span>
                                을 확인하였으며 이에 동의합니다(필수)
                            </Form.Label>                   
                        </FormGroup>
                        <Button className="inputSubmit" type="button" onClick={submitData} style={{position:'absolute',top:'914px',left:'458px',width:'421px',height:'65px', borderRadius:'33px'}}><span className="inputSubmit-text">신청하기</span></Button>
                    </Form>       
                </Col>	    
		    </Container> 
        </div>
    );
}
export default DataInput;


