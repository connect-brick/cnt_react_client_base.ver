import React from 'react';
import './App.css';
import Header from './Component/Header'
import Mainbanner from './Component/Mainbanner'
import Section1 from './Component/Section1'
import Section2 from './Component/Section2'
import Section3 from './Component/Section3'
import Section4 from './Component/Section4'
import Section5 from './Component/Section5'
import PartnerSection from './Component/PartnerSection'
import DataInput from './Component/DataInput'
import Section6 from './Component/Section6'
import Footer from './Component/Footer'
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  return (
    <div className="App">
        <Header />
        <Mainbanner />  
        <Section1 />
        <Section2 />
        <Section3 />
        <Section4 />
        <Section5 />
        <PartnerSection />
        <DataInput />
        <Section6 />
        <Footer />
    </div>
  );
}
export default App;
